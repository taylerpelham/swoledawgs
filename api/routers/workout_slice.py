from fastapi import (
    Depends,
    Response,
    APIRouter,
)
from typing import Union, List
from queries.workout_slice import WorkoutSliceQueries
from models.accounts import AccountOut
from models.errors import HttpError
from models.workout_slice import (
    WorkoutSliceIn,
    WorkoutSliceOut,
    WorkoutSliceUpdate,
)
from authenticator import authenticator

router = APIRouter()


@router.get(
    "/api/workout_slice/users_last",
    response_model=Union[WorkoutSliceOut, HttpError, None],
)
async def get_users_last_workout_slice(
    response: Response,
    workout_queries: WorkoutSliceQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[WorkoutSliceOut, HttpError, None]:
    if account:
        workout_slice = workout_queries.get_users_last_workout(
            user_id=account["id"]
        )
        if workout_slice is None:
            return None
        if type(workout_slice) is HttpError:
            response.status_code = 404
            return workout_slice
        return workout_slice
    response.status_code = 401
    return HttpError(detail="User not logged in.")


@router.get(
    "/api/workout_slice/{id}", response_model=Union[WorkoutSliceOut, HttpError]
)
async def get_workout_slice(
    id: int,
    response: Response,
    workout_queries: WorkoutSliceQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[WorkoutSliceOut, HttpError]:
    if account:
        workout_slice = workout_queries.get_one(id=id)
        if type(workout_slice) is HttpError:
            response.status_code = 404
            return workout_slice
        return workout_slice
    response.status_code = 401
    return HttpError(detail="User not logged in.")


@router.get(
    "/api/workout_slice",
    response_model=Union[List[WorkoutSliceOut], HttpError],
)
async def list_workout_slices(
    response: Response,
    workout_slices: WorkoutSliceQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[List[WorkoutSliceOut], HttpError]:
    if account:
        workout_slice_list = workout_slices.get()
        if type(workout_slice_list) is HttpError:
            response.status_code = 404
            return workout_slice_list
        return workout_slice_list
    response.status_code = 401
    return HttpError(detail="User not logged in.")


@router.post(
    "/api/workout_slice", response_model=Union[WorkoutSliceOut, HttpError]
)
async def create_workout_slice(
    info: WorkoutSliceIn,
    response: Response,
    workout_query: WorkoutSliceQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[WorkoutSliceOut, HttpError]:
    if account:
        workout_slice = workout_query.create(
            workout_slice=info, account_id=account["id"]
        )
        if type(workout_slice) is HttpError:
            response.status_code = 404
            return workout_slice
        return workout_slice
    response.status_code = 401
    return HttpError(detail="User not logged in")


@router.put(
    "/api/workout_slice/{id}", response_model=Union[WorkoutSliceOut, HttpError]
)
def update_workout_slice(
    workout_slice_id: int,
    response: Response,
    workout_slice: WorkoutSliceUpdate,
    repo: WorkoutSliceQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[WorkoutSliceOut, HttpError]:

    if account:
        updated_workout_slice = repo.update(workout_slice_id, workout_slice)
        if type(updated_workout_slice) is HttpError:
            response.status_code = 404
            return updated_workout_slice
        return updated_workout_slice
    response.status_code = 401
    return HttpError(detail="User not logged in")


@router.delete(
    "/api/workout_slice/{id}", response_model=Union[bool, HttpError]
)
def delete_workout_slice(
    workout_slice_id: int,
    response: Response,
    workout_slice: WorkoutSliceQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[bool, HttpError]:

    if account:
        is_deleted = workout_slice.delete(workout_slice_id)
        if type(is_deleted) is HttpError:
            response.status_code = 404
            return is_deleted
        return is_deleted
    response.status_code = 401
    return HttpError(detail="User not logged in")
