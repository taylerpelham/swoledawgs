from fastapi import (
    Depends,
    Response,
    APIRouter,
)

from queries.workouts import WorkoutQueries
from models.accounts import AccountOut
from typing import Union, List
from models.workouts import WorkoutOut, WorkoutInById
from authenticator import authenticator
from models.errors import HttpError


router = APIRouter()


@router.post("/api/workouts", response_model=Union[WorkoutOut, HttpError])
def create_workout(
    info: WorkoutInById,
    response: Response,
    workouts: WorkoutQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[WorkoutOut, HttpError]:
    if account:
        workout = workouts.create(workout=info, account_id=account["id"])
        if type(workout) is HttpError:
            response.status_code = 404
            return workout
        return workout
    response.status_code = 401
    return HttpError(detail="User not logged in.")


@router.get("/api/workouts/{id}", response_model=Union[WorkoutOut, HttpError])
def get_workout(
    id: int,
    response: Response,
    workouts: WorkoutQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[WorkoutOut, HttpError]:
    if account:
        workout = workouts.get_one(id=id)
        if type(workout) is HttpError:
            response.status_code = 404
            return workout
        return workout
    response.status_code = 401
    return HttpError(detail="User not logged in.")


@router.get("/api/workouts", response_model=Union[List[WorkoutOut], HttpError])
async def list_workouts(
    response: Response,
    workouts: WorkoutQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[List[WorkoutOut], HttpError]:
    if account:
        workouts_list = workouts.get_all()
        if type(workouts_list) is HttpError:
            response.status_code = 404
            return workouts_list
        return workouts_list
    response.status_code = 401
    return HttpError(detail="User not logged in.")


@router.put("/api/workouts/{id}", response_model=Union[WorkoutOut, HttpError])
def update_workout(
    workout_id: int,
    response: Response,
    workout: WorkoutInById,
    repo: WorkoutQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[WorkoutOut, HttpError]:
    if account:
        updated_workout = repo.update(workout_id, workout)
        if type(updated_workout) is HttpError:
            response.status_code = 404
            return updated_workout
        return updated_workout
    response.status_code = 401
    return HttpError(detail="User not logged in.")


@router.delete("/api/workouts/{id}", response_model=Union[bool, HttpError])
def delete_workout(
    workout_id: int,
    response: Response,
    workout: WorkoutQueries = Depends(),
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> Union[bool, HttpError]:
    if account:
        result = workout.delete(workout_id)
        if type(result) is HttpError:
            response.status_code = 404
            return result
        return result
    response.status_code = 401
    return HttpError(detail="User not logged in.")
