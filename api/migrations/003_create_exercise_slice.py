steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE exercise_slice (
            
            id SERIAL PRIMARY KEY NOT NULL, 
            exercise_id INTEGER NOT NULL REFERENCES exercise(id),
            sets INTEGER NOT NULL,
            reps INTEGER NOT NULL,
            is_complete BOOL DEFAULT false,
            completed_on TIMESTAMP DEFAULT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE exercise_slice;
        """,
    ],
]
