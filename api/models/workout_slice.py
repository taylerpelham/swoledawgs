from pydantic import BaseModel
from models.workouts import WorkoutOut
from datetime import datetime
from typing import Optional, Union


class WorkoutSliceIn(BaseModel):
    workout_id: int
    scheduled: Optional[datetime]
    notes: Optional[str]
    is_complete: Optional[bool]


class WorkoutSliceOut(BaseModel):
    workout_slice_id: int
    workout: WorkoutOut
    date_complete: Optional[Union[datetime, None]]
    scheduled: Optional[Union[datetime, None]]
    notes: Optional[Union[str, None]]
    is_complete: bool
    user_id: Optional[Union[int, None]]


class WorkoutSliceUpdate(BaseModel):
    workout_id: int
    date_complete: datetime
    scheduled: Optional[datetime]
    notes: Optional[str]
    is_complete: bool
