from pydantic import BaseModel
from jwtdown_fastapi.authentication import Token


class AccountForm(BaseModel):
    username: str
    password: str


class AccountOut(BaseModel):
    id: str
    email: str
    full_name: str
    height_in_inches: int
    weight_in_pounds: int
    sex: str


class AccountToken(Token):
    account: AccountOut


class AccountIn(BaseModel):
    email: str
    password: str
    full_name: str
    height_in_inches: int
    weight_in_pounds: int
    sex: str


# Whats presented to the Client (React, Swagger)


class AccountOutWithPassword(AccountOut):
    hashed_password: str  # random string

    # hashed pass is what gets saved to DB
