from pydantic import BaseModel
from models.exercise_slice import ExerciseSliceOut
from typing import List, Union, Optional


class WorkoutIn(BaseModel):
    name: str
    exercises: List[ExerciseSliceOut]


class WorkoutInById(BaseModel):
    name: str
    exercise_ids: List[int]


class WorkoutOut(WorkoutIn):
    id: int
    user_id: Optional[Union[int, None]]
    ppl: Optional[Union[str, None]]
