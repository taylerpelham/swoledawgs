from models.workouts import WorkoutOut, WorkoutInById
from models.exercise_slice import ExerciseSliceOut
from queries.exercise_slice import ExerciseSliceQueries
from models.errors import HttpError
from .pool import pool
from typing import Union, List
from statistics import mode


class WorkoutQueries:
    def get_all(self) -> Union[List[WorkoutOut], HttpError]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, name, user_id, ppl
                        FROM workout
                        ORDER BY name;
                        """
                    )
                    workouts = []
                    workout_records = db.fetchall()
                    for record in workout_records:
                        exercise_slices_result = db.execute(
                            """
                        SELECT exercise_slice_id
                        FROM workout_exercises
                        WHERE workout_id = %s
                        """,
                            [record[0]],
                        )
                        exercise_slices = self.get_exercise_slice_list(
                            exercise_slices_result
                        )

                        workouts.append(
                            WorkoutOut(
                                id=record[0],
                                name=record[1],
                                exercises=exercise_slices,
                                user_id=record[2],
                                ppl=record[3],
                            )
                        )
                    return workouts
        except Exception as e:
            print(e)
            return HttpError(detail="Could not return Workouts")

    def get_one(self, id: int) -> Union[WorkoutOut, HttpError]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    workout_result = db.execute(
                        """
                        SELECT name, user_id, ppl
                        FROM workout
                        WHERE id = %s
                        """,
                        [id],
                    )
                    workout = workout_result.fetchone()
                    exercise_slices_result = db.execute(
                        """
                        SELECT exercise_slice_id
                        FROM workout_exercises
                        WHERE workout_id = %s
                        """,
                        [id],
                    )
                    exercise_slice_outs = self.get_exercise_slice_list(
                        exercise_slices_result
                    )

                    return WorkoutOut(
                        id=id,
                        name=workout[0],
                        exercises=exercise_slice_outs,
                        user_id=workout[1],
                        ppl=workout[2],
                    )
        except Exception as e:
            print(e)
            return HttpError(detail="Could not return workout")

    def create(
        self, workout: WorkoutInById, account_id: int
    ) -> Union[WorkoutOut, HttpError]:
        exercise_slice_queries = ExerciseSliceQueries()
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    workout_result = db.execute(
                        """
                        INSERT INTO workout
                            (name, user_id)
                        VALUES
                            (%s, %s)
                        RETURNING id;
                        """,
                        [workout.name, account_id],
                    )

                    workout_id = workout_result.fetchone()[0]

                    exercise_slice_list = []
                    ppl_list = []
                    for exercise_slice_id in workout.exercise_ids:
                        db.execute(
                            """
                            INSERT INTO workout_exercises
                                (workout_id, exercise_slice_id)
                            VALUES
                                (%s, %s)
                            RETURNING id;
                            """,
                            [workout_id, exercise_slice_id],
                        )

                        exercise_slice = exercise_slice_queries.get_one(
                            id=exercise_slice_id
                        )
                        ppl_list.append(exercise_slice.exercise.body_group)
                        exercise_slice_list.append(exercise_slice)
                    ppl = mode(ppl_list)
                    db.execute(
                        """
                        UPDATE workout
                        SET
                            ppl = %s
                        WHERE id = %s
                        """,
                        [ppl, workout_id],
                    )
                    return WorkoutOut(
                        id=workout_id,
                        exercises=exercise_slice_list,
                        name=workout.name,
                        user_id=account_id,
                        ppl=ppl,
                    )
        except Exception as e:
            print(e)
            return HttpError(detail="Could create workout")

    def update(
        self, workout_id: int, workout: WorkoutInById
    ) -> Union[WorkoutOut, HttpError]:
        exercise_slice_queries = ExerciseSliceQueries()
        try:
            is_deleted = self.get_one(workout_id)
            if is_deleted is None:
                return HttpError(detail="Workout doesn't exist")
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE workout
                        SET
                            name = %s
                        WHERE id = %s
                        """,
                        [workout.name, workout_id],
                    )

                    exercise_slices_result = db.execute(
                        """
                        SELECT exercise_slice_id
                        FROM workout_exercises
                        WHERE workout_id = %s
                        """,
                        [workout_id],
                    )
                    ids = [id[0] for id in exercise_slices_result]

                    exercise_outs = [
                        exercise_slice_queries.get_one(id) for id in ids
                    ]

                    if ids == workout.exercise_ids:
                        return WorkoutOut(
                            id=workout_id,
                            name=workout.name,
                            exercises=exercise_outs,
                        )
                    db.execute(
                        """
                        DELETE FROM workout_exercises
                        WHERE workout_id = %s
                        """,
                        [workout_id],
                    )
                    exercise_slice_list = []
                    for exercise_slice_id in workout.exercise_ids:
                        db.execute(
                            """
                            INSERT INTO workout_exercises
                                (workout_id, exercise_slice_id)
                            VALUES
                                (%s, %s)
                            RETURNING id;
                            """,
                            [workout_id, exercise_slice_id],
                        )

                        exercise_slice = exercise_slice_queries.get_one(
                            id=exercise_slice_id
                        )
                        exercise_slice_list.append(exercise_slice)
                    return WorkoutOut(
                        id=workout_id,
                        name=workout.name,
                        exercises=exercise_slice_list,
                    )
        except Exception as e:
            print(e)
            return HttpError(detail="Workout cannot be updated")

    def delete(self, workout_id: int) -> Union[bool, HttpError]:
        try:
            is_deleted = self.get_one(workout_id)
            if is_deleted is None:
                return False

            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM workout_exercises
                        WHERE workout_id = %s
                        """,
                        [workout_id],
                    )
                    db.execute(
                        """
                        DELETE FROM workout
                        WHERE id = %s
                        """,
                        [workout_id],
                    )

                    return True
        except Exception as e:
            print(e)
            return HttpError(detail="Workout could not be deleted")

    def get_exercise_slice_list(
        self, exercise_slice_ids
    ) -> List[ExerciseSliceOut]:
        exercise_slice_list = []
        exercise_slice_queries = ExerciseSliceQueries()
        for exercise_slice_id in exercise_slice_ids:
            exercise_slice_out = exercise_slice_queries.get_one(
                exercise_slice_id[0]
            )
            exercise_slice_list.append(exercise_slice_out)
        return exercise_slice_list
