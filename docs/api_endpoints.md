### Accounts

Allows for the creation, update, and deletion of accounts.
Account Endpoints:
| Method | Path                          | Description                       |
|--------|-------------------------------|-----------------------------------|
| GET    | /api/token/                   | Returns current logged in account |
| POST   | /api/accounts/                | Creates an account                |
| PUT    | /api/accounts/{account_email} | Updates an account                |
| DELETE | /api/accounts/{account_email} | Deletes an account                |
#### Create an Account
Input:
```json
{
  "email": "bubba@gmail.com",
  "password": "bubbaspassword",
  "full_name": "Bubba Bubba",
  "height_in_inches": 76,
  "weight_in_pounds": 265,
  "sex": "male"
}
```

Output:
```json
{
  "access_token": "ACCESS_TOKEN",
  "token_type": "Bearer",
  "account": {
    "id": "2",
    "email": "bubba@gmail.com",
    "full_name": "Bubba Bubba",
    "height_in_inches": 76,
    "weight_in_pounds": 265,
    "sex": "male"
  }
}
```
### Exercise Endpoints:
| Method | Path                | Description                                                  |
|--------|---------------------|--------------------------------------------------------------|
| GET    | /api/exercises/     | Returns a list of exercises                                  |
| GET    | /api/exercises/{id} | Returns an exercise by Id                                    |
| POST   | /api/exercises/     | Creates exercises from ExerciseDB API, all of these are prefilled into the database |
Over 1200 exercises come prefilled in the Database.
#### Example exercise:
```json
{
    "name": "assisted standing triceps extension (with towel)",
    "body_group": "push",
    "body_part": "upper arms",
    "equipment": "assisted",
    "pic_url": "https://v2.exercisedb.io/image/OOCbrxY6xQF3WG",
    "target": {
      "name": "triceps"
    },
    "descriptions": "Stand with your feet shoulder-width apart and hold a towel with both hands behind your head. Keep your elbows close to your ears and your upper arms stationary. Slowly extend your forearms upward, squeezing your triceps at the top. Pause for a moment, then slowly lower the towel back down to the starting position. Repeat for the desired number of repetitions. ",
    "id": 18
  }
```

### Exercise Slice Endpoints:
Exercise Slices are how we track sets and reps for Exercises. 
| Method | Path                     | Description                       |
|--------|--------------------------|-----------------------------------|
| GET    | /api/exercise_slice/     | Returns a list of exercise slices |
| POST   | /api/exercise_slice/     | Creates an exercise slices        |
| DELETE | /api/exercise_slice/{id} | Deletes an exercise slice         |
| GET    | /api/exercise_slice/{id} | Returns an exercise slice         |
| PUT    | /api/exercise_slice/{id} | Updates an exercise slice         |
### Create an exercise slice
Input:
``` json
{
  "exercise_id": 7,
  "sets": 3,
  "reps": 10
}
```

Output:
```json
{
  "id": 1,
  "exercise": {
    "name": "alternate lateral pulldown",
    "body_group": "pull",
    "body_part": "back",
    "equipment": "cable",
    "pic_url": "https://v2.exercisedb.io/image/Uv0Z9PWffiKK3U",
    "target": {
      "name": "lats"
    },
    "descriptions": "Sit on the cable machine with your back straight and feet flat on the ground. Grasp the handles with an overhand grip, slightly wider than shoulder-width apart. Lean back slightly and pull the handles towards your chest, squeezing your shoulder blades together. Pause for a moment at the peak of the movement, then slowly release the handles back to the starting position. Repeat for the desired number of repetitions. ",
    "id": 7
  },
  "sets": 3,
  "reps": 10,
  "is_complete": null,
  "completed_on": null
}
```



### Workout Endpoints:
| Method | Path               | Description                |
|--------|--------------------|----------------------------|
| GET    | /api/workouts/     | Returns a list of workouts |
| POST   | /api/workouts/     | Creates a workout          |
| DELETE | /api/workouts/{id} | Deletes a workout          |
| GET    | /api/workouts/{id} | Gets a workout by its Id   |
| PUT    | /api/workouts/{id} | Updates a workout          |
#### Create a Workout:
A workout is built from a list of exercise slice ids and a name.

Input:
```json
{
  "name": "Legagaddon",
  "exercise_ids": [
    51, 52, 53, 54
  ]
}
```

Output:
```json
{
  "name": "Legagaddon",
  "exercises": [
    {
      "id": 51,
      "exercise": {
        "name": "barbell front squat",
        "body_group": "legs",
        "body_part": "upper legs",
        "equipment": "barbell",
        "pic_url": "https://v2.exercisedb.io/image/i7b2Y9PZCunybV",
        "target": {
          "name": "glutes"
        },
        "descriptions": "Start by standing with your feet shoulder-width apart, toes slightly turned out. Hold the barbell in front of your shoulders, resting it on your collarbone and shoulders. Engage your core and keep your chest up as you lower your body down into a squat position, pushing your hips back and bending your knees. Lower until your thighs are parallel to the ground, or as low as you can comfortably go. Pause for a moment at the bottom, then push through your heels to return to the starting position. Repeat for the desired number of repetitions. ",
        "id": 42
      },
      "sets": 4,
      "reps": 10,
      "is_complete": false,
      "completed_on": null
    },
    {
      "id": 52,
      "exercise": {
        "name": "barbell good morning",
        "body_group": "legs",
        "body_part": "upper legs",
        "equipment": "barbell",
        "pic_url": "https://v2.exercisedb.io/image/rdNeReq1qpEVsU",
        "target": {
          "name": "hamstrings"
        },
        "descriptions": "Start by standing with your feet shoulder-width apart and the barbell resting on your upper back. Keeping your back straight and your core engaged, hinge forward at the hips, pushing your buttocks back as if you were trying to touch the wall behind you with your glutes. Lower your torso until it is parallel to the ground, feeling a stretch in your hamstrings. Pause for a moment, then return to the starting position by squeezing your glutes and pushing your hips forward. Repeat for the desired number of repetitions. ",
        "id": 44
      },
      "sets": 3,
      "reps": 10,
      "is_complete": false,
      "completed_on": null
    },
    {
      "id": 53,
      "exercise": {
        "name": "barbell lateral lunge",
        "body_group": "legs",
        "body_part": "upper legs",
        "equipment": "barbell",
        "pic_url": "https://v2.exercisedb.io/image/gYK91Lx7s3hvl2",
        "target": {
          "name": "glutes"
        },
        "descriptions": "Stand with your feet shoulder-width apart, holding a barbell across your upper back. Take a big step to the side with your right foot, keeping your left foot planted. Bend your right knee and lower your body down into a lunge position, keeping your left leg straight. Push off with your right foot and return to the starting position. Repeat on the other side, stepping with your left foot. ",
        "id": 1410
      },
      "sets": 4,
      "reps": 12,
      "is_complete": false,
      "completed_on": null
    },
    {
      "id": 54,
      "exercise": {
        "name": "barbell seated calf raise",
        "body_group": "legs",
        "body_part": "lower legs",
        "equipment": "barbell",
        "pic_url": "https://v2.exercisedb.io/image/Dn1dWFNfQ1avSb",
        "target": {
          "name": "calves"
        },
        "descriptions": "Sit on a bench with your feet flat on the floor and a barbell resting on your thighs. Place the balls of your feet on a raised platform, such as a block or step. Position the barbell across your thighs and hold it securely with your hands. Keeping your back straight and your core engaged, lift your heels off the ground by extending your ankles. Pause for a moment at the top, then slowly lower your heels back down to the starting position. Repeat for the desired number of repetitions. ",
        "id": 88
      },
      "sets": 3,
      "reps": 10,
      "is_complete": false,
      "completed_on": null
    }
  ],
  "id": 4,
  "user_id": 1,
  "ppl": "legs"
}

```

### Workout Slice Endpoints:
| Method | Path                         | Description                                          |
|--------|------------------------------|------------------------------------------------------|
| GET    | /api/workout_slice/          | Returns a list of Workout Slices(Completed Workouts) |
| POST   | /api/workout_slice/          | Creates Workout Slices                               |
| DELETE | /api/workout_slice/{id}      | Deleted Workout Slices                               |
| GET    | /api/workout_slice/{id}      | Get a Workout Slice by its Id                        |
| PUT    | /api/workout_slice/{id}      | Update a Workout Slice                               |
| GET    | /api/workout_slice/user_last | Returns the Users Last Workout Slice                 |
Workout Slices are how we track completed workouts. 

#### Create a Workout Slice
Input:
```json
{
  "workout_id": 4,
  "notes": "I hope I'll be able to walk tomorrow!",
  "is_complete": true
}
```

Output:
```json
{
  "workout_slice_id": 1,
  "workout": {
    "name": "Legagaddon",
    "exercises": [
      {
        "id": 51,
        "exercise": {
          "name": "barbell front squat",
          "body_group": "legs",
          "body_part": "upper legs",
          "equipment": "barbell",
          "pic_url": "https://v2.exercisedb.io/image/i7b2Y9PZCunybV",
          "target": {
            "name": "glutes"
          },
          "descriptions": "Start by standing with your feet shoulder-width apart, toes slightly turned out. Hold the barbell in front of your shoulders, resting it on your collarbone and shoulders. Engage your core and keep your chest up as you lower your body down into a squat position, pushing your hips back and bending your knees. Lower until your thighs are parallel to the ground, or as low as you can comfortably go. Pause for a moment at the bottom, then push through your heels to return to the starting position. Repeat for the desired number of repetitions. ",
          "id": 42
        },
        "sets": 4,
        "reps": 10,
        "is_complete": false,
        "completed_on": null
      },
      {
        "id": 52,
        "exercise": {
          "name": "barbell good morning",
          "body_group": "legs",
          "body_part": "upper legs",
          "equipment": "barbell",
          "pic_url": "https://v2.exercisedb.io/image/rdNeReq1qpEVsU",
          "target": {
            "name": "hamstrings"
          },
          "descriptions": "Start by standing with your feet shoulder-width apart and the barbell resting on your upper back. Keeping your back straight and your core engaged, hinge forward at the hips, pushing your buttocks back as if you were trying to touch the wall behind you with your glutes. Lower your torso until it is parallel to the ground, feeling a stretch in your hamstrings. Pause for a moment, then return to the starting position by squeezing your glutes and pushing your hips forward. Repeat for the desired number of repetitions. ",
          "id": 44
        },
        "sets": 3,
        "reps": 10,
        "is_complete": false,
        "completed_on": null
      },
      {
        "id": 53,
        "exercise": {
          "name": "barbell lateral lunge",
          "body_group": "legs",
          "body_part": "upper legs",
          "equipment": "barbell",
          "pic_url": "https://v2.exercisedb.io/image/gYK91Lx7s3hvl2",
          "target": {
            "name": "glutes"
          },
          "descriptions": "Stand with your feet shoulder-width apart, holding a barbell across your upper back. Take a big step to the side with your right foot, keeping your left foot planted. Bend your right knee and lower your body down into a lunge position, keeping your left leg straight. Push off with your right foot and return to the starting position. Repeat on the other side, stepping with your left foot. ",
          "id": 1410
        },
        "sets": 4,
        "reps": 12,
        "is_complete": false,
        "completed_on": null
      },
      {
        "id": 54,
        "exercise": {
          "name": "barbell seated calf raise",
          "body_group": "legs",
          "body_part": "lower legs",
          "equipment": "barbell",
          "pic_url": "https://v2.exercisedb.io/image/Dn1dWFNfQ1avSb",
          "target": {
            "name": "calves"
          },
          "descriptions": "Sit on a bench with your feet flat on the floor and a barbell resting on your thighs. Place the balls of your feet on a raised platform, such as a block or step. Position the barbell across your thighs and hold it securely with your hands. Keeping your back straight and your core engaged, lift your heels off the ground by extending your ankles. Pause for a moment at the top, then slowly lower your heels back down to the starting position. Repeat for the desired number of repetitions. ",
          "id": 88
        },
        "sets": 3,
        "reps": 10,
        "is_complete": false,
        "completed_on": null
      }
    ],
    "id": 4,
    "user_id": 1,
    "ppl": "legs"
  },
  "date_complete": "2024-03-22T18:46:57.140297",
  "scheduled": null,
  "notes": "string",
  "is_complete": true,
  "user_id": 1
}
```