import { Outlet } from 'react-router-dom'
import Nav from './app/components/Nav'
import Footer from './app/components/Footer'



const App = () => {
    return (
        <div>
            <Nav />

            <Outlet />

            <Footer />
        </div>
    )
}

export default App
