const Footer = () => {

    return (
        <footer className="footer text-white text-center p-3">
            <img src="../swole_dawgz.png" alt="Gym Logo" className="footer-img" />
            <p>
                &copy; {new Date().getFullYear()} SWOLEBRUHS. All rights
                reserved.
            </p>
        </footer>
    )
}

export default Footer
