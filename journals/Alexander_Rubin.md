# JOURNAL FOR ALEXANDER RUBIN: SWOLE BRUHS

# FRIDAY 03/22/2024

Group accomplishments:
- Tayler and I used flake8 and black to clean the backend.

Personal accomplishments:
- I ran a successful test last night and pushed it to main.

Notes:
- It's the final day of the project.  We haven't turned it in yet, but hoping to this afternoon.


# THURSDAY 03/21/2024

Group accomplishments:
- we fixed account invalidator tags, which fixed a problem with the register component not redirecting to the unlogged in landing page
- coded a delete mechanism for removing an exercise_slice from the workout create page.
- added a message when the exercise_slice was added to the workout.
- we made it so all the the workout list, and the calendar pages automatically refreshed when a workout or workout_slice was created, respectively.

Personal contribution:
- I paired with Tayler and Abrahim, and we all contributed to the product of today.


# WEDNESDAY 03/20/2024

Group accomplishments:
- Tayler, Abrahim, and I grouped up on creating the algorithm for the recommended workout component.
- We finished the calendar page by making sure it only displayed workout_slices created by the logged in user.
- Andrew and Jeongjae paired on the readme.
- Riley helped us figure out how to pack the database with CSVs for exercises, exercise_slices, and workouts, so that the a new user already has some pre packaged workouts to run through, and so the user doesn't have to hit the 3rd party API.

Personal accomplishments:
- Code that I generated while building the workout list was retrofitted to work on the calendar view for the workout_slices.
- We did a lot of pairing.  I don't know if credit or blame can be assigned to only one of us for any of the features we built.

Notes:
-Whenever it seems like we're done, something ugly pops up.


# TUESDAY 03/19/2024

Group accomplishments:
- After much deliberation, we got the calendar page to show only the workout slices performed by the current user.
- We started working on prefilling the database with exercises, exercise_slices, and workouts.

Personal accomplishments:
- I generated the code that isolated the workout slices on the calendar page to only those done by the current user.

Notes:
- 


# MONDAY 03/18/2024

Group accomplishments:
- I paired with Tayler to set up the links, making sure the CreateWorkout, WorkoutList, and CreateWorkoutSlice pages all talk to each other appropriately. We ended the day blocked on how to get only a specific user's workouts to display on the WorkoutList page. The others mobbed the calendar view, and other various styling initiatives.

Personal accomplishments:
- Tayler and I worked well together on this.  There was a lot of discussion about the logic, and how to make this meet the MVP.

Notes:
- I was surprised to see the calendar page the other group created.  I think they did a great job!
- Tayler and I work well together. I'm uncertain if we're using redux correctly. If the website works, then it works, but there is the need to refresh sometimes, and that shouldn't happen.



# THRUSDAY 03/14/2024

Group accomplishments:
- We broke into pairs today. I worked with Andrew on the current workout page.
- Abrahim and Jeongjae continued working on the Login and Logout functions, as well as a landing page.

Personal accomplishments:
- I was blocked on how to edit a workout slice, then realized that it wasn't necessary to edit them. I could simply post one. I don't think I ever used console.log so much. I realized that the backend queries and models had to be modified in order for me to do what I needed to do.  I was able to create the workout slice, but the 'notes', 'is_complete' and 'date_complete' fields were null or false, which was not what I wanted. The backend workoutSliceIn model was not able to accomodate the change, so I changed the model, and then had to modify the query to reflect the change in the model.

Notes:
- I was blocked for a long time.  I wanted to avoid altering the backend because we spent so much time getting it right, but it was necessary, and now it works. We usually start each day riddled with silly git frustrations.


# WEDNESDAY 03/13/2024

Group accomplishments:
- we split up into groups. Abrahim, Andrew and Jeongjae started working on Login, Logout.
- Tayler and I set up the user registration page.

Personal contribution:
- I mostly worked on styling in the jsx area while Talyer mostly worked on the logic, however there was crossover both ways. 


# TUESDAY 03/12/2024

Group accomplishments:
- we mob coded to get redux set up.
- started getting front end authentication set up.


# MONDAY 03/11/2024

Group accomplishments:
- we sorted out the error handling.
- started working on setting up redux.

Personal contribution:
- I drove for the error handling, and did a merge request.

Notes:
- Daylight Savings Time made everyone tired


# FRIDAY 03/08/2024

Group accomplishments:
- we finished backout workout slices, thus finishing all backend routes.
- started working on error handling, got blocked.


# THURSDAY 03/07/2024

Group accomplishments:
- we finished workout.
- started working on workout slices.


# WEDNESDAY 03/06/2024

Group accomplishments:
- we finished exercise slices.
- started working on workout

Personal contributtion:
-

Notes


# TUESDAY 03/05/2024

Group accomplishments:
- we worked primarily on exercise slices. 
- we finished the routers and queries for create, get one, get all, and delete.

Personal contribution:
- I did a lot of the actual writing.
- I pushed to git two times.
- I used the vacation example project as a reference.

Notes:
- It was mostly smooth. When making the delete query/router, swagger kept returning true for objects that didn't exist.  We spent a lot of time trying to make that work properly.

# MONDAY 03/04/2024

Group accomplishments:
- we finished the models, routers, and queries for exercises.  We are able to access the exercises we want from the API based on the body_group (push, pull, legs) or the body part. 
- we started working on the models, routers and queries for exercise_slice.
- there was a lot of discussion about the logic and funtionality of the exercises, and exercise slices in the context of our project, and we have a more unified vision of how it all works.

Personal contribution:
- I actively participated in the issue/ticket creation.
- I coded most of the query for exercise_slice, while pairing with Tayler, but it isn't finished yet.

Notes:
- Andrew was still not with us. The last time we saw him was Friday morning, but he was gone for most of the day after that, and we haven't heard back. He said he had a serious emergency to deal with and we all hope he is okay. 
- I must remember that everyone, at least at this stage, is using a reference when coding. When we started the mod project work for the day, I was already feeling drained, and I didn't understand how my teammates simply knew what to write. It didn't occur to me, until after I asked, that they all have seperate monitors that aren't shared, that have the vacation example project on it and they are more or less copying the code.  This is a totally acceptable practice, but not realizing that they were all using a reference made me feel behind.



# FRIDAY 03/01/2024

Group accomplishments:
- we finished backend authorizations.
- we started coming up with stories to help germinate issues that we have to turn into tickets.
- we began working on the exercise model, and towards the end of the day, we managed to get a functioning third party API call to RapidAPI to access their database of exercises.

Personal contribution:
- I did a fair amount of driving. I'm finding that I enjoy the talking about the logic and function of the code. Many of the ideas that I come up with are adopted, and many aren't. I am certainly playing an important role in the ideation for each step of the way.

Notes:
- I find that we are doing mostly mob coding rather than pairing. I think it puts a lot of the leadership weight on Tayler, as he seems to be the most experienced. I don't think he minds, but I also don't want the group to be so heavily dependent on him.


# THURSDAY 02/29/2024

Group accomplishments:
- we continued working on the backend authorization. We finished the CRUD for accounts.
- we encountered a git issue, but Murphy helped us solve it.

Personal contribution:
- I got us unstuck a few times. We were having trouble getting the sign up function to give a 200 and realized that in order for it to work, the login query had to also be complete. We all took turns driving (typing in the shared VS Code).

Notes:
- Andrew left abruptly. Not sure if was in response to something we said. We worked smoothly despite his absence. 


# WEDNESDAY 02/28/2024

Group accomplishments:
- we continued working on the backend authorization
- we reorganized our code so that all the pydantic models were in the same place
- we used the recording from Riley's lecture, and the video from Learn as a reference

Personal contribution:
- I encouraged us to move all the pydantic models to the same file.
- I acted as typist for a chunk of the afternoon.
- I pushed 'pre db push' to the UserAuth branch

Notes:
- I feel responsible for wasting a lot of time by encouraging the pydantic model move.
- We are still getting our sea legs for all of this, patience is a virtue.


# TUESDAY 02/27/2024

Group accomplishments:
- we received our repo from gitlab
- we began working on backend authorization: we followed the video from Learn.

Personal contribution:
- we each took turns either sharing our screen and typing into VS code.

Notes:
- we learned that VS code can be buggy when live sharing, if we are all trying to type at the same time.
- the process was smooth, but slow. When we finished we arrived at our API endpoints meeting the expected 401 error.


# MONDAY 02/26/2024

Group accomplishments:
- today we presented our wireframe and swole bruhs was approved by Dalonte.

Personal contribution:
- I acted as spokesperson and led the presentation of our wireframe to the class and teachers.

Notes:
- n/a