<details>
<summary>Table of Contents</summary>

[TOC]

</details>

# Day 1 2-27-24 FAST API BACKEND AUTH JWTDOWN

Today, our group worked on:

* FAST API BACK END AUTH USING JWTDOWN, GALVANIZE's OWN BACKEND AUTHENTICATION LIBRARY

We mobb coded the beginnings of the authentication following the JWTDOWN tutorial. We coded together utilizing Zoom screen share and VS Code LiveShare. Teamwork make the dream work!

We have not implemented the database yet.

README work in progress

ISSUES first one OPEN

Journal started

MOD3 PROJECT SWOLE BROTHERS IS UNDERWAY!

# Day 2 2-28-24 JWTDOWN

Today, our group worked on:

* FAST API BACK END USING JWTDOWN

Standup to keep track of progress, future plans, and blockers

Continued mobb programming backend authentication utilizing Zoom screen share and VS Code LiveShare.

Simultaneously learning backend authentication and implementing it may seem like we are moving slowly but we are making steady progress and laying the foundation for one of the most important aspects to many if not all Web Applications: User Authentication. Due to many cyber security threats and creative hackers, we need to ensure successful integration of User Authentication. This skill will carry over into employment where we will need to adjust quickly to their system of user authentication or potentially even implement a new user authentication. This skill of rapid learning and implementation will translate very usefully into software engineering employment.

We imported Authenticator and Token and other built in functions and methods from the JWTDown Library. We coded our Accounts router, Accounts queries, Accounts models around the JWTDown provided functionality to code our backend Authentication that is ready to interact with the database. Our authentication endpoints have been tested on FASTAPI DOCS SWAGGER and are no longer giving 500 Server Errors, they are giving 401 errors and we are ready to implement database interactions for CRUD.

We added to psycopg with binary and pool capabilities to our requirements.txt.
We added an external volume in our docker-compose.yml to map a connection to the PGSQL database and allow data to persist beyond the container.
We added our database micro service to our docker-compose.yml utilizing PostgreSQL, assigning ports and username and password.
We provided a database URL for the API back end micro service which will be used to make pool connections to our database to perform CRUD API operations as necessary.

Next Steps: We will begin to integrate POSTGRESQL database connections in order to code our repo/queries functions. We will use PSYCOPG POOL capabilities to allow multiple simultaneous connections to the PostgreSQL database to implement backend UserAuthentication involving migrating tables and creating and inserting Users to the database with SQL DDL and DML when users sign up to use our Web App.
