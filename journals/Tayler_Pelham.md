⠀FRIDAY 03/22/2024
Team Progress:
* Completed and submitted the project

THURSDAY 03/21/2024
Group Achievements:
* Team divided tasks, with Andrew, and Jeongjae focusing on the README, while Alex , Abrahim and I worked through adding tags, invalidating them and fixing countless small issues in our code, such as styling issues, backend auth issues, token issues etc. The MVP is completed with Work still needing to be done on the README and we need to fix the submission time to local time. Its currently in Zulu time.

⠀Contribution Insights:
* I helped with logic of the queries, filters etc. 

Notes:
* We undid the global state of the user, Riley instructed us to not user Redux persist which we needed to allow users to not be erased from the redux state if a user refreshes while logged in.

⠀WEDNESDAY 03/20/2024
Team Progress:
* Began integrating user state into the store

Contribution Insights:
* I lead the team in the implantation of redux, creating the slice adding it to the store etc.

Notes:
* I try to explain my thought process to the team while I code while not taking too much time, I’m not sure if this helps them or not. 

⠀TUESDAY 03/19/2024
Team Progress:
* 

MONDAY 03/18/2024
Collaborative Progress:
* Partnered with Alex to establish interlinking functionalities across CreateWorkout, WorkoutList, and CreateWorkoutSlice pages, ensuring seamless communication. We encountered a hurdle in displaying only a specific user's workouts on the WorkoutList page by day's end. Meanwhile, the rest of the team focused on enhancing the calendar view and various styling enhancements.

⠀Individual Highlights:
* Alex and I worked effectively on this task, engaging in extensive deliberations on logic to align with MVP standards.

⠀Observations:
* My collaboration with Alex during the Redux implementation went well. Ensuring seamless website functionality remains paramount, with occasional refresh requirements needing attention.

⠀WEDNESDAY 03/13/2024
Group Achievements:
* Team divided tasks, with Abrahim, Andrew, and Jeongjae focusing on login functionalities, while Alex and I spearheaded the user registration page setup.

⠀Contribution Insights:
* Alex lead in frontend styling within JSX while I took the lead on logic implementation. Effective collaboration facilitated seamless task execution.

⠀TUESDAY 03/12/2024
Team Progress:
* Employed mob coding for Redux setup and initiated frontend authentication setup.

⠀MONDAY 03/11/2024
Team Progress:
* Resolved error handling issues and commenced Redux setup.

⠀Individual Involvement:
* Led the charge on error handling and facilitated merge requests, contributing actively to the team's progress.

⠀Observations:
* Daylight Savings Time took its toll on team energy levels.

⠀FRIDAY 03/08/2024
Team Achievements:
* Completed backend workout slice functionalities, overcoming challenges in error handling.

⠀THURSDAY 03/07/2024
Team Progress:
* Finalized workout functionalities and commenced work on workout slices.

⠀WEDNESDAY 03/06/2024
Team Milestones:
* Successfully concluded exercise slice functionalities and transitioned to workout tasks.

⠀TUESDAY 03/05/2024
Team Accomplishments:
* Advanced exercise slice functionalities, encountering minor challenges in Swagger integration.

⠀Personal Input:
* Took charge of writing tasks and actively participated in version control.

⠀Observations:
* Smooth progress marred by minor integration issues.

⠀MONDAY 03/04/2024
Team Milestones:
* Completed exercise model functionalities and commenced work on exercise slice functionalities.

⠀Personal Input:
* Actively participated in issue creation and contributed to exercise slice query development.


⠀FRIDAY 03/01/2024
Team Achievements:
* Concluded backend authorizations and initiated ideation for upcoming tasks.

⠀Personal Role:
* Played a significant role in ideation discussions, contributing valuable insights. 


⠀THURSDAY 02/29/2024
Team Progress:
* Advanced backend authorization tasks, overcoming technical hurdles.

⠀Personal Involvement:
* Played a pivotal role in troubleshooting and ensuring task completion amid team dynamics.


⠀WEDNESDAY 02/28/2024
Team Milestones:
* Made progress in backend authorization tasks and streamlined code organization.

⠀Personal Contribution:
* Initiated code reorganization and actively contributed to version control.

⠀Observations:
* Acknowledged the need for patience and adaptability amidst evolving team dynamics.

⠀TUESDAY 02/27/2024
Team Progress:
* Initiated backend authorization tasks, aligning with instructional materials.

⠀Personal Input:
* Collaboratively engaged in coding sessions, sharing responsibilities.

⠀Observations:
* Identified challenges with live sharing tools, emphasizing the need for effective communication.

⠀MONDAY 02/26/2024
Accomplishments:
* Successfully presented wireframe, securing approval from Dalonte.

⠀Individual Contribution:
* Building out pages and endpoint design on the wireframe.
